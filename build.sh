#!/bin/bash

cp gitlab-ci.yml.header /tmp/build/.gitlab-ci.yml

curl -sL https://github.com/shyiko/jabba/raw/master/install.sh | bash && . ~/.jabba/jabba.sh
#| JABBA_COMMAND="ls-remote" bash | grep \@ > /tmp/jabba.out

jabba ls-remote | grep \@ > /tmp/jabba.out
while read p; do
  echo "JABBA JDK: $p"
  TAG=${p//@/-}
  VERSION="$(cut -d'@' -f2 <<<"$p")"
  JDK="$(cut -d'@' -f1 <<<"$p")"

  echo "JDK: $JDK"
  echo "VERSION: $VERSION"

  echo "build:$1-$TAG:" >> /tmp/build/.gitlab-ci.yml
  echo "  <<: *build_job_$1" >> /tmp/build/.gitlab-ci.yml	
  echo "  variables:" >> /tmp/build/.gitlab-ci.yml
  echo "    JDKVERSION: $p" >> /tmp/build/.gitlab-ci.yml
  echo "    VERSION: $VERSION" >> /tmp/build/.gitlab-ci.yml
  echo "    JDK: $JDK" >> /tmp/build/.gitlab-ci.yml
  echo "" >> /tmp/build/.gitlab-ci.yml

done </tmp/jabba.out

