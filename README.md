# infinitenature/jdk

Build docker images with jabba jdks installed. The result is stored at [docker-hub](https://hub.docker.com/r/infinitenature/jdk/).

This repo creates docker images with different jdks installed. It uses the [jabba](https://github.com/shyiko/jabba/) java package manager to install
them.

## Nameing

The tag for the image is build this way: `FLAVOR-JDK_VENDOR-VERSION`

For each available jdk there are several flavors of the image:

  * `buildpack-dep` is based on `buildpack-deps:18.04`
  * `buildpack-scm` is based on `buildpack-deps:18.04-scm`
  * `buildpack-curl` is based on `buildpack-deps:18.04-curl`

## Build

Because gitlab-ci, which is used to build, can't modify the current configuration this repo creates a build configuration and pushed it to [an ohter repo](https://git.loe.auf.uni-rostock.de/werbeo/docker/jabba-jdk-build). From there the images are pushed to [docker-hub](https://hub.docker.com/r/infinitenature/jdk/)

## Updates

The images will be rebuild once a week, so new jdk versions supported by jabba should be soon also on docker-hub.
